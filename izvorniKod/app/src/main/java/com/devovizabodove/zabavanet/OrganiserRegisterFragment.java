package com.devovizabodove.zabavanet;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.devovizabodove.zabavanet.presenters.OrganiserRegisterPresenter;
import com.google.firebase.auth.FirebaseUser;


public class OrganiserRegisterFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private EditText nameOrganiserRegisterEditText;
    private EditText addressOrganiserRegisterEditText;
    private EditText emailOrganiserRegisterEditText;
    private EditText passwordOrganiserRegisterEditText;
    private Button registerOrganiserButton;
    private ProgressDialog dialog;
    private OrganiserRegisterPresenter organiserRegisterPresenter;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public OrganiserRegisterFragment() {
        // Required empty public constructor
    }

    public void registrationPaymentO(final FirebaseUser user){
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getActivity(), OrganiserPaymentActivity.class).putExtra("user", user));
            }
        }, 1000);
    }


    public void registrationSuccessO(){
        nameOrganiserRegisterEditText = getActivity().findViewById(R.id.nameOrganiserRegisterEditText);
        addressOrganiserRegisterEditText = getActivity().findViewById(R.id.addressOrganiserRegisterEditText);
        emailOrganiserRegisterEditText = getActivity().findViewById(R.id.emailOrganiserRegisterEditText);
        passwordOrganiserRegisterEditText = getActivity().findViewById(R.id.passwordOrganiserRegisterEditText);
        dialog.dismiss();
        Toast.makeText(getActivity(), "Registration successful, proceed to payment.", Toast.LENGTH_SHORT).show();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                nameOrganiserRegisterEditText.setText("");
                addressOrganiserRegisterEditText.setText("");
                emailOrganiserRegisterEditText.setText("");
                passwordOrganiserRegisterEditText.setText("");
            }
        });
    }

    public void registrationFailed(){
        dialog.dismiss();
        Toast.makeText(getActivity(), "Registration failed.", Toast.LENGTH_SHORT).show();
        addressOrganiserRegisterEditText.setText("");
        emailOrganiserRegisterEditText.setText("");
        passwordOrganiserRegisterEditText.setText("");
    }

    public void registrationFailExistsO(){
        dialog.dismiss();
        emailOrganiserRegisterEditText = getActivity().findViewById(R.id.emailOrganiserRegisterEditText);
        passwordOrganiserRegisterEditText = getActivity().findViewById(R.id.passwordOrganiserRegisterEditText);
        Toast.makeText(getActivity(), "Email already exists.", Toast.LENGTH_SHORT).show();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                emailOrganiserRegisterEditText.setText("");
                passwordOrganiserRegisterEditText.setText("");
            }
        });
    }

    public void nameErrorEmptyO(){
        nameOrganiserRegisterEditText = getActivity().findViewById(R.id.nameOrganiserRegisterEditText);
        passwordOrganiserRegisterEditText = getActivity().findViewById(R.id.passwordOrganiserRegisterEditText);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                nameOrganiserRegisterEditText.setError("Name is required");
                nameOrganiserRegisterEditText.requestFocus();
            }
        });
    }

    public void emailErrorEmptyO(){
        emailOrganiserRegisterEditText = getActivity().findViewById(R.id.emailOrganiserRegisterEditText);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                emailOrganiserRegisterEditText.setError("Email is required");
                emailOrganiserRegisterEditText.requestFocus();
            }
        });
    }

    public void addressErrorEmptyO(){
        addressOrganiserRegisterEditText = getActivity().findViewById(R.id.addressOrganiserRegisterEditText);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                addressOrganiserRegisterEditText.setError("Address is required");
                addressOrganiserRegisterEditText.requestFocus();
            }
        });
    }

    public void passwordErrorEmptyO(){
        passwordOrganiserRegisterEditText = getActivity().findViewById(R.id.passwordOrganiserRegisterEditText);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                passwordOrganiserRegisterEditText.setError("Password is required");
                passwordOrganiserRegisterEditText.requestFocus();
            }
        });
    }

    public void emailErrorPatternO(){
        emailOrganiserRegisterEditText = getActivity().findViewById(R.id.emailOrganiserRegisterEditText);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                emailOrganiserRegisterEditText.setError("Please enter a valid email");
                emailOrganiserRegisterEditText.requestFocus();
            }
        });
    }

    public void passwordErrorLengthO(){
        passwordOrganiserRegisterEditText = getActivity().findViewById(R.id.passwordOrganiserRegisterEditText);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                passwordOrganiserRegisterEditText.setError("Minimum length of password is 6");
                passwordOrganiserRegisterEditText.requestFocus();
                passwordOrganiserRegisterEditText.setText("");
            }
        });
    }

    public void showProgressBar() {
        dialog = ProgressDialog.show(getActivity(), "",
                "Please wait, you are being registered.", true);
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OrganiserRegisterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OrganiserRegisterFragment newInstance(String param1, String param2) {
        OrganiserRegisterFragment fragment = new OrganiserRegisterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_organiser_register, container, false);

        organiserRegisterPresenter = new OrganiserRegisterPresenter(this);

        registerOrganiserButton = (Button) view.findViewById(R.id.registerOrganiserButton);

        registerOrganiserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nameOrganiserRegisterEditText = getActivity().findViewById(R.id.nameOrganiserRegisterEditText);
                String name = nameOrganiserRegisterEditText.getText().toString().trim();
                addressOrganiserRegisterEditText = getActivity().findViewById(R.id.addressOrganiserRegisterEditText);
                String address = addressOrganiserRegisterEditText.getText().toString().trim();
                emailOrganiserRegisterEditText = getActivity().findViewById(R.id.emailOrganiserRegisterEditText);
                String email = emailOrganiserRegisterEditText.getText().toString().trim();
                passwordOrganiserRegisterEditText = getActivity().findViewById(R.id.passwordOrganiserRegisterEditText);
                String password = passwordOrganiserRegisterEditText.getText().toString().trim();

                organiserRegisterPresenter.checkCredentials(name, address, email,password);
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }



}
