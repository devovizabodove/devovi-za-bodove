package com.devovizabodove.zabavanet;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class EventFeedVisitorActivity extends AppCompatActivity{

    String[] testEventNames = {"Event1"};
    String[] testEventOrganisers = {"EventOrganiser1"};
    String[] testEventTime = {"4:20 PM"};
    String[] testEventLocation = {"Bedekovcina"};
    int[] testImages = {R.drawable.credit_card_logo};
    final String[] spinner_choices = {"No filter", "Next 24 hours", "Next 7 days", "Next 30 days"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_feed_visitor);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.navigation_home);

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        break;
                    case R.id.navigation_profile:
                        Intent a = new Intent(EventFeedVisitorActivity.this,VisitorProfileActivity.class);
                        startActivity(a);
                        break;
                }
                return false;
            }
        });

        Spinner spinner = findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(EventFeedVisitorActivity.this,
                android.R.layout.simple_spinner_item,spinner_choices);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.v("item", (String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });


        ListView mListView = findViewById(R.id.eventsList);
        EventAdapter eventAdapter = new EventAdapter(EventFeedVisitorActivity.this, testImages,
                testEventNames, testEventOrganisers, testEventTime, testEventLocation);
        mListView.setAdapter(eventAdapter);


        TextView noItemsFound = findViewById(R.id.noItemsFoundTextView);

        if (eventAdapter.getCount() == 0){
            noItemsFound.setVisibility(View.VISIBLE);
        }
        else{
            noItemsFound.setVisibility(View.GONE);
        }
    }


}
