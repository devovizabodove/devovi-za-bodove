package com.devovizabodove.zabavanet.presenters;

import android.util.Patterns;

import com.devovizabodove.zabavanet.LoginActivity;
import com.devovizabodove.zabavanet.models.LoginModel;
import com.google.firebase.firestore.DocumentSnapshot;

public class LoginPresenter {
    private LoginModel loginModel;
    private LoginActivity loginView;

    public  LoginPresenter(LoginActivity loginView) {
        this.loginView = loginView;
        this.loginModel = new LoginModel(this);
    }


    public void checkCredentials(String email, String password){
        int flag = 0;

        if(email.isEmpty()) {
            loginView.emailErrorEmptyO();
            flag = 1;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            loginView.emailErrorPatternO();
            flag = 1;
        }

        if(password.isEmpty()) {
            loginView.passwordErrorEmptyO();
            flag = 1;
        }

        if(password.length() < 6) {
            loginView.passwordErrorLengthO();
            flag = 1;
        }
        if(flag == 0){
            loginModel.setEmail(email);
            loginModel.setPassword(password);

            loginModel.login();
        }
    }

    public void loginOutcome(Object o) {
        if(o instanceof String)
            loginView.loginSuccess((String) o);
        else loginView.loginFail();
    }

    public void onAnimationEnd() {
        loginModel.isUserLoggedIn();
    }

    public void onUserDataComplete(DocumentSnapshot documentSnapshot) {
        if(documentSnapshot != null && documentSnapshot.getData() != null)
            loginView.redirectToHomeScreen(documentSnapshot.getData());
        else loginView.showLoginForm();
    }
}
