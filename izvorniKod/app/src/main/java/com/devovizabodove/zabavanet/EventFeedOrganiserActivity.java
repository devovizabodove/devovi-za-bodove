package com.devovizabodove.zabavanet;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class EventFeedOrganiserActivity extends AppCompatActivity{

    String[] testEventNames = {"Event1"};
    String[] testEventOrganisers = {"EventOrganiser1"};
    String[] testEventTime = {"4:20 PM"};
    String[] testEventLocation = {"Bedekovcina"};
    int[] testImages = {R.drawable.credit_card_logo};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_feed_organiser);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.navigation_home);

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        break;
                    case R.id.navigation_profile:
                        Intent a = new Intent(EventFeedOrganiserActivity.this,OrganiserProfileActivity.class);
                        startActivity(a);
                        break;
                }
                return false;
            }
        });


        ListView mListView = findViewById(R.id.eventsList);
        EventAdapter eventAdapter = new EventAdapter(EventFeedOrganiserActivity.this, testImages,
                testEventNames, testEventOrganisers, testEventTime, testEventLocation);
        mListView.setAdapter(eventAdapter);

        FloatingActionButton createEventFab = findViewById(R.id.fab);
        createEventFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create event
            }
        });

        TextView noItemsFound = findViewById(R.id.noItemsFoundTextView);

        if (eventAdapter.getCount() == 0){
            noItemsFound.setVisibility(View.VISIBLE);
        }
        else{
            noItemsFound.setVisibility(View.GONE);
        }
    }


}
