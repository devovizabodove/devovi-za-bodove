package com.devovizabodove.zabavanet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SendPasswordResetCodeActivity extends AppCompatActivity {

    Button sendEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_reset);
        sendEmail = findViewById(R.id.resetPassword);
        sendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SendPasswordResetCodeActivity.this, ResetPasswordActivity.class));
            }
        });
    }
}
