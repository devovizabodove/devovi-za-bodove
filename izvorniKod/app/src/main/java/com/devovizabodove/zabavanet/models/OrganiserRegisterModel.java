package com.devovizabodove.zabavanet.models;

import android.util.Log;

import androidx.annotation.NonNull;

import com.devovizabodove.zabavanet.presenters.OrganiserRegisterPresenter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.content.ContentValues.TAG;

public class OrganiserRegisterModel {
    private Map<String, Object> organiser = new HashMap<>();

    private String password;

    private OrganiserRegisterPresenter organiserPresenter;

    public OrganiserRegisterModel(OrganiserRegisterPresenter organiserPresenter) {
        this.organiserPresenter = organiserPresenter;
        organiser.put("user-type", "organiser");
    }

    public void registerOrganiser() {
        final FirebaseAuth mAuth = FirebaseAuth.getInstance();

        final String email = (String) organiser.get("email");

        if (email != null && password != null) {
            mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        FirebaseFirestore db = FirebaseFirestore.getInstance();
                        db.collection("users")
                                .document(Objects.requireNonNull(mAuth.getCurrentUser()).getUid())
                                .set(organiser).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d(TAG, "DocumentSnapshot added");
                                organiserPresenter.registrationOutcome(mAuth.getCurrentUser());
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "Error adding document", e);
                                organiserPresenter.registrationOutcome(null);
                            }
                        });
                    } else if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                        organiserPresenter.userCollision();
                    }
                }
            });
        }

    }


    public String getName() {
        return (String) organiser.get("name");
    }

    public String getAddress() {
        return (String) organiser.get("address");
    }

    public String getEmail() {
        return (String) organiser.get("email");
    }

    public void setName(String name) {
        organiser.put("name", name);
    }

    public void setAddress(String address) {
        organiser.put("adress", address);
    }

    public void setEmail(String email) {
        organiser.put("email", email);
    }

    public void setPassword(String password){
        this.password = password;
    }
}
