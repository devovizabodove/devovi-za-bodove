package com.devovizabodove.zabavanet;

import android.os.Bundle;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class OrganiserReviewsActivity extends AppCompatActivity {

    String[] visitorNamesTest = {"test1", "test2", "test3"};
    String[] visitorDatesTest = {"test1Dates", "test2Dates", "test3Dates"};
    String[] visitorReviewsTest = {"test1Reviews", "test2Reviews", "test3Reviews"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organiser_reviews);

        ListView mListView = findViewById(R.id.reviewOrganiserView);
        ReviewAdapter reviewAdapter = new ReviewAdapter(OrganiserReviewsActivity.this,
                visitorNamesTest, visitorDatesTest, visitorReviewsTest);
        mListView.setAdapter(reviewAdapter);
    }
}
