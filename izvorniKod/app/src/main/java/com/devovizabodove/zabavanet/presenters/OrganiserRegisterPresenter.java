package com.devovizabodove.zabavanet.presenters;

import android.util.Patterns;

import com.devovizabodove.zabavanet.OrganiserRegisterFragment;
import com.devovizabodove.zabavanet.models.OrganiserRegisterModel;
import com.google.firebase.auth.FirebaseUser;

import javax.annotation.Nonnull;

public class OrganiserRegisterPresenter {
    private OrganiserRegisterModel organiserModel;
    private OrganiserRegisterFragment organiserView;

    public OrganiserRegisterPresenter(OrganiserRegisterFragment organiserView) {
        this.organiserView = organiserView;
        this.organiserModel = new OrganiserRegisterModel(this);
    }

    public void registrationOutcome(FirebaseUser user){
        if (user != null) {
            organiserView.registrationSuccessO();
            organiserView.registrationPaymentO(user);
        }
        else {
            organiserView.registrationFailed();
            }
        }


    public String checkCredentials(@Nonnull String name, @Nonnull String address , @Nonnull String email, @Nonnull String password){
        int flag = 0;
        String returnValue = "";

        if(name.isEmpty()){
            organiserView.nameErrorEmptyO();
            flag = 1;
            returnValue = "Name is required";
        }

        if(address.isEmpty()) {
            organiserView.addressErrorEmptyO();
            flag = 1;
            returnValue = "Address is required";
        }

        if(email.isEmpty()) {
            organiserView.emailErrorEmptyO();
            flag = 1;
            returnValue = "Email is required";
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            organiserView.emailErrorPatternO();
            flag = 1;
            returnValue = "Please enter a valid email";
        }

        if(password.isEmpty()) {
            organiserView.passwordErrorEmptyO();
            flag = 1;
            returnValue = "Password is required";
        }

        if(password.length() < 6) {
            organiserView.passwordErrorLengthO();
            flag = 1;
            returnValue = "Minimum length of password is 6";
        }
        if(flag == 0){
            organiserModel.setName(name);
            organiserModel.setAddress(address);
            organiserModel.setEmail(email);
            organiserModel.setPassword(password);
            organiserView.showProgressBar();
            organiserModel.registerOrganiser();
            returnValue = "Registration successful, proceed to payment.";
        }
        return returnValue;
    }

    public void userCollision() {
        organiserView.registrationFailExistsO();
    }

}


