package com.devovizabodove.zabavanet;

import android.os.Bundle;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class OrganiserGalleryViewActivity extends AppCompatActivity {

    int[] test_images = {R.drawable.img1, R.drawable.img2, R.drawable.img3, R.drawable.img4};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organiser_gallery_view);

        ListView mListView = findViewById(R.id.organiserGalleryView);
        GalleryAdapter galleryAdapter = new GalleryAdapter(OrganiserGalleryViewActivity.this, test_images);
        mListView.setAdapter(galleryAdapter);
    }
}
