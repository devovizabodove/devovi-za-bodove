package com.devovizabodove.zabavanet;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class VisitorProfileActivity extends AppCompatActivity {
    String[] testEventNames = {"Event1"};
    String[] testEventOrganisers = {"EventOrganiser1"};
    String[] testEventTime = {"4:20 PM"};
    String[] testEventLocation = {"Bedekovcina"};
    int[] testImages = {R.drawable.credit_card_logo};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_profile);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.navigation_profile);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        Intent a = new Intent(VisitorProfileActivity.this,EventFeedVisitorActivity.class);
                        startActivity(a);
                        break;
                    case R.id.navigation_profile:
                        break;
                }
                return false;
            }
        });
        ListView mListView = findViewById(R.id.UserEventHistoryList);
        EventAdapter eventAdapter = new EventAdapter(VisitorProfileActivity.this, testImages,
                testEventNames, testEventOrganisers, testEventTime, testEventLocation);
        mListView.setAdapter(eventAdapter);
    }
}
