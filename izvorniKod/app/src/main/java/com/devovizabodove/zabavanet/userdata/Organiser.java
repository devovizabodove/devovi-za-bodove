package com.devovizabodove.zabavanet.userdata;

public class Organiser extends User {
    private String address;

    public Organiser(String name, String email, String address, String password) {
        super(name, email, password);
        this.address = address;
    }

    public String getAddress() {
        return address;
    }
}
