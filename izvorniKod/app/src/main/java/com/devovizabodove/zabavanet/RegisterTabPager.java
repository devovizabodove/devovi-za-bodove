package com.devovizabodove.zabavanet;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class RegisterTabPager extends FragmentStatePagerAdapter {

    public RegisterTabPager(FragmentManager fm){
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return new VisitorRegisterFragment();
            case 1: return new OrganiserRegisterFragment();
        }
        return new VisitorRegisterFragment();
    }
    @Override
    public int getCount() {
        return 2;
    }
    @Override    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0: return "Visitor";
            case 1: return "Organiser";
            default: return null;
        }
    }
}
