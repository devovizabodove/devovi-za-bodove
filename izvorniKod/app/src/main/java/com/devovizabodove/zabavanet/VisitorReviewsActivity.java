package com.devovizabodove.zabavanet;

import android.os.Bundle;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class VisitorReviewsActivity extends AppCompatActivity {

    String[] visitorNamesTest = {"test1", "test2", "test3"};
    String[] visitorDatesTest = {"test1Dates", "test2Dates", "test3Dates"};
    String[] visitorReviewsTest = {"test1Reviews", "test2Reviews", "test3Reviews"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_reviews);

        ListView mListView = findViewById(R.id.reviewVisitorView);
        ReviewAdapter reviewAdapter = new ReviewAdapter(VisitorReviewsActivity.this,
                visitorNamesTest, visitorDatesTest, visitorReviewsTest);
        mListView.setAdapter(reviewAdapter);
    }
}
