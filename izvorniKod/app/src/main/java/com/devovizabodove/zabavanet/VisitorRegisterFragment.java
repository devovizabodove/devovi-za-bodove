package com.devovizabodove.zabavanet;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.devovizabodove.zabavanet.presenters.VisitorRegisterPresenter;
import com.google.firebase.auth.FirebaseUser;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;


public class VisitorRegisterFragment extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private EditText nameVisitorRegisterEditText;
    private EditText emailVisitorRegisterEditText;
    private EditText passwordVisitorRegisterEditText;
    private Button registerVisitorButton;

    private ProgressDialog dialog;

    private VisitorRegisterPresenter visitorRegisterPresenter;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public VisitorRegisterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VisitorRegisterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VisitorRegisterFragment newInstance(String param1, String param2) {
        VisitorRegisterFragment fragment = new VisitorRegisterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_visitor_register, container, false);

        visitorRegisterPresenter = new VisitorRegisterPresenter(this);

        registerVisitorButton = view.findViewById(R.id.registerVisitorButton);

        registerVisitorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nameVisitorRegisterEditText = Objects.requireNonNull(getActivity()).findViewById(R.id.nameVisitorRegisterEditText);
                String name = nameVisitorRegisterEditText.getText().toString().trim();
                emailVisitorRegisterEditText = getActivity().findViewById(R.id.emailVisitorRegisterEditText);
                String email = emailVisitorRegisterEditText.getText().toString().trim();
                passwordVisitorRegisterEditText = getActivity().findViewById(R.id.passwordVisitorRegisterEditText);
                String password = passwordVisitorRegisterEditText.getText().toString();

                visitorRegisterPresenter.checkCredentials(name, email, password);
            }
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    public void registrationSuccess(final FirebaseUser user) {
        dialog.dismiss();
        nameVisitorRegisterEditText = Objects.requireNonNull(getActivity()).findViewById(R.id.nameVisitorRegisterEditText);
        emailVisitorRegisterEditText = getActivity().findViewById(R.id.emailVisitorRegisterEditText);
        passwordVisitorRegisterEditText = getActivity().findViewById(R.id.passwordVisitorRegisterEditText);
        Toast.makeText(getActivity(), "Registration successful.", Toast.LENGTH_SHORT).show();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                nameVisitorRegisterEditText.setText("");
                emailVisitorRegisterEditText.setText("");
                passwordVisitorRegisterEditText.setText("");
            }
        });

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                startActivity(new Intent(getActivity(), EventFeedVisitorActivity.class).putExtra("user", user));
            }
        }, 1000);
    }

    public void registrationFailedExists() {
        dialog.dismiss();
        passwordVisitorRegisterEditText = getActivity().findViewById(R.id.passwordVisitorRegisterEditText);
        Toast.makeText(getActivity(), "Email already exists.", Toast.LENGTH_SHORT).show();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                passwordVisitorRegisterEditText.setText("");
            }
        });
    }

    public void registrationFailed() {
        dialog.dismiss();
        emailVisitorRegisterEditText = getActivity().findViewById(R.id.emailVisitorRegisterEditText);
        passwordVisitorRegisterEditText = getActivity().findViewById(R.id.passwordVisitorRegisterEditText);
        Toast.makeText(getActivity(), "Registration failed.", Toast.LENGTH_SHORT).show();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                emailVisitorRegisterEditText.setText("");
                passwordVisitorRegisterEditText.setText("");
            }
        });
    }

    public void nameErrorEmpty() {
        nameVisitorRegisterEditText = Objects.requireNonNull(getActivity()).findViewById(R.id.nameVisitorRegisterEditText);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                nameVisitorRegisterEditText.setError("Name is required");
                nameVisitorRegisterEditText.requestFocus();
            }
        });
    }

    public void emailErrorEmpty() {
        emailVisitorRegisterEditText = getActivity().findViewById(R.id.emailVisitorRegisterEditText);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                emailVisitorRegisterEditText.setError("Email is required");
                emailVisitorRegisterEditText.requestFocus();
            }
        });
    }


    public void emailErrorPattern() {
        emailVisitorRegisterEditText = getActivity().findViewById(R.id.emailVisitorRegisterEditText);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                emailVisitorRegisterEditText.setError("Please enter a valid email");
                emailVisitorRegisterEditText.requestFocus();
            }
        });
    }

    public void passwordErrorEmpty() {
        passwordVisitorRegisterEditText = getActivity().findViewById(R.id.passwordVisitorRegisterEditText);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                passwordVisitorRegisterEditText.setError("Password is required");
                passwordVisitorRegisterEditText.requestFocus();
            }
        });
    }

    public void passwordErrorLength() {
        passwordVisitorRegisterEditText = getActivity().findViewById(R.id.passwordVisitorRegisterEditText);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                passwordVisitorRegisterEditText.setError("Minimum length of password is 6");
                passwordVisitorRegisterEditText.requestFocus();
                passwordVisitorRegisterEditText.setText("");
            }
        });
    }

    public void showProgressBar() {
        dialog = ProgressDialog.show(getActivity(), "",
                "Please wait, you are being registered.", true);
    }
}
