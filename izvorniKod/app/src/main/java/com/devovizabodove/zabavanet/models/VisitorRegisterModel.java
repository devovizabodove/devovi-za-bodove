package com.devovizabodove.zabavanet.models;

import android.util.Log;

import com.devovizabodove.zabavanet.presenters.VisitorRegisterPresenter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class VisitorRegisterModel {
    private Map<String, Object> visitor = new HashMap<>();

    private String password;

    private VisitorRegisterPresenter visitorRegisterPresenter;

    public VisitorRegisterModel(VisitorRegisterPresenter visitorRegisterPresenter) {
        this.visitorRegisterPresenter = visitorRegisterPresenter;
        visitor.put("user-type", "visitor");
    }

    public void registerVisitor() {
        final FirebaseAuth mAuth = FirebaseAuth.getInstance();

        final String email = (String) visitor.get("email");

        if(email != null && password != null) {
            mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        FirebaseFirestore db = FirebaseFirestore.getInstance();
                        db.collection("users")
                                .document(Objects.requireNonNull(mAuth.getCurrentUser()).getUid())
                                .set(visitor).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d(TAG, "DocumentSnapshot added");
                                visitorRegisterPresenter.registrationOutcome(mAuth.getCurrentUser());
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "Error adding document", e);
                                visitorRegisterPresenter.registrationOutcome(null);
                            }
                        });
                    } else if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                        visitorRegisterPresenter.userCollision();
                    }
                }
            });
        }
    }

    public String getName() {
        return (String) visitor.get("name");
    }

    public void setName(String name) {
        visitor.put("name", name);
    }

    public String getEmail() {
        return (String) visitor.get("email");
    }

    public void setEmail(String email) {
        visitor.put("email", email);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
