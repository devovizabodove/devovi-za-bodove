package com.devovizabodove.zabavanet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

public class EventAdapter extends ArrayAdapter<String> {

    private String[] eventNames;
    private int[] eventImages;
    private String[] eventOrganisers;
    private String[] eventTime;
    private String[] eventLocation;
    private Context mContext;

    public EventAdapter(@NonNull Context context,int[] eventImages, String[] eventNames,
                        String[] eventOrganisers, String[] eventTime, String[] eventLocation) {
        super(context, R.layout.events_list_item);
        mContext = context;
        this.eventImages = eventImages;
        this.eventNames = eventNames;
        this.eventOrganisers = eventOrganisers;
        this.eventTime = eventTime;
        this.eventLocation = eventLocation;
    }

    @Override
    public int getCount(){
        return eventNames.length;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        ViewHolder mViewHolder = new ViewHolder();
        if (convertView==null){
            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.events_list_item, parent, false);
            mViewHolder.eventName = convertView.findViewById(R.id.EventName);
            mViewHolder.eventImage = convertView.findViewById(R.id.EventImage);
            mViewHolder.eventOrganiser = convertView.findViewById(R.id.EventOrganiserName);
            mViewHolder.eventLocation = convertView.findViewById(R.id.EventLocation);
            mViewHolder.eventTime = convertView.findViewById(R.id.EventTime);
            convertView.setTag(mViewHolder);
        }
        else{
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        mViewHolder.eventName.setText(eventNames[position]);
        mViewHolder.eventOrganiser.setText(eventOrganisers[position]);
        mViewHolder.eventLocation.setText(eventLocation[position]);
        mViewHolder.eventTime.setText(eventTime[position]);
        if (eventImages[position] != 0) {
            mViewHolder.eventImage.setImageResource(eventImages[position]);
        }

        return convertView;
    }

    static class ViewHolder{
        TextView eventName;
        TextView eventOrganiser;
        ImageView eventImage;
        TextView eventTime;
        TextView eventLocation;
    }
}
