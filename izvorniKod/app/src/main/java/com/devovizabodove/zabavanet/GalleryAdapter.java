package com.devovizabodove.zabavanet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import androidx.annotation.NonNull;

public class GalleryAdapter extends ArrayAdapter<String> {

    private int[] galleryImages;
    private Context mContext;

    public GalleryAdapter(@NonNull Context context,int[] galleryImages) {
        super(context, R.layout.gallery_img_item);
        mContext = context;
        this.galleryImages = galleryImages;
    }

    @Override
    public int getCount(){
        return galleryImages.length;
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent){
        ViewHolder mViewHolder = new ViewHolder();
        if (convertView==null){
            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.gallery_img_item, parent, false);
            mViewHolder.galleryImage = convertView.findViewById(R.id.EventImage);
            convertView.setTag(mViewHolder);
        }
        else{
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        mViewHolder.galleryImage.setImageResource(galleryImages[position]);


        return convertView;
    }

    static class ViewHolder{
        ImageView galleryImage;
    }
}
