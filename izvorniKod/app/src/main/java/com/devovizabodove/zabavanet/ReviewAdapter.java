package com.devovizabodove.zabavanet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

public class ReviewAdapter extends ArrayAdapter<String> {

    private String[] visitorNames;
    private String[] dateTimes;
    private String[] visitorReviews;
    private Context mContext;

    public ReviewAdapter(@NonNull Context context, String[] visitorNames, String[] dateTimes, String[] visitorReviews) {
        super(context, R.layout.review_item);
        mContext = context;
        this.visitorNames = visitorNames;
        this.dateTimes = dateTimes;
        this.visitorReviews = visitorReviews;
    }

    @Override
    public int getCount(){
        return visitorNames.length;
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent){
        ViewHolder mViewHolder = new ViewHolder();
        if (convertView==null){
            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.review_item, parent, false);
            mViewHolder.visitorName = convertView.findViewById(R.id.txtVisitorName);
            mViewHolder.dateTime = convertView.findViewById(R.id.txtDate);
            mViewHolder.visitorReview = convertView.findViewById(R.id.txtReview);
            convertView.setTag(mViewHolder);
        }
        else{
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        mViewHolder.visitorName.setText(visitorNames[position]);
        mViewHolder.dateTime.setText(dateTimes[position]);
        mViewHolder.visitorReview.setText(visitorReviews[position]);


        return convertView;
    }

    static class ViewHolder{
        TextView visitorName;
        TextView dateTime;
        TextView visitorReview;
    }
}
