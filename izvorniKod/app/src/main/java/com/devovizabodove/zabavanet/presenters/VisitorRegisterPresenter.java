package com.devovizabodove.zabavanet.presenters;

import android.util.Patterns;

import com.devovizabodove.zabavanet.VisitorRegisterFragment;
import com.devovizabodove.zabavanet.models.VisitorRegisterModel;
import com.google.firebase.auth.FirebaseUser;

import javax.annotation.Nonnull;

public class VisitorRegisterPresenter {
    private final VisitorRegisterModel visitorRegisterModel;
    private final VisitorRegisterFragment visitorRegisterFragment;

    public VisitorRegisterPresenter(VisitorRegisterFragment visitorRegisterFragment) {
        this.visitorRegisterFragment = visitorRegisterFragment;
        this.visitorRegisterModel = new VisitorRegisterModel(this);
    }

    public void registrationOutcome(FirebaseUser user) {
        if (user != null) visitorRegisterFragment.registrationSuccess(user);
        else visitorRegisterFragment.registrationFailed();
    }

    public String checkCredentials(@Nonnull String name, @Nonnull String email, @Nonnull String password) {
        int flag = 0;
        String returnValue = "";

        if (name.isEmpty()) {
            visitorRegisterFragment.nameErrorEmpty();
            flag = 1;
            returnValue = "Name is required";
        }

        if (email.isEmpty()) {
            visitorRegisterFragment.emailErrorEmpty();
            flag = 1;
            returnValue = "Email is required";
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            visitorRegisterFragment.emailErrorPattern();
            flag = 1;
            returnValue = "Please enter a valid email";
        }

        if (password.isEmpty()) {
            visitorRegisterFragment.passwordErrorEmpty();
            flag = 1;
            returnValue = "Password is required";
        }

        if (password.length() < 6) {
            visitorRegisterFragment.passwordErrorLength();
            flag = 1;
            returnValue = "Minimum length of password is 6";
        }

        if (flag == 0) {
            visitorRegisterModel.setName(name);
            visitorRegisterModel.setEmail(email);
            visitorRegisterModel.setPassword(password);
            visitorRegisterFragment.showProgressBar();
            visitorRegisterModel.registerVisitor();
            returnValue = "Please wait, you are being registered.";
        }
        return returnValue;
    }

    public void userCollision() {
        visitorRegisterFragment.registrationFailedExists();
    }
}
