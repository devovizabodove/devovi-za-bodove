package com.devovizabodove.zabavanet;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.devovizabodove.zabavanet.presenters.LoginPresenter;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

public class LoginActivity extends AppCompatActivity {

    private ProgressBar loadingProgressBar;
    private RelativeLayout rootView, afterAnimationView;
    private ImageView zabavaNetLogo;
    private TextView register, forgotPassword;
    private EditText emailEditText, passwordEditText;
    private Button loginButton;
    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        initViews();

        loginPresenter = new LoginPresenter(this);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, SendPasswordResetCodeActivity.class));
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginUser();
            }
        });


        
        new CountDownTimer(5000, 1000) {

            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                loadingProgressBar.setVisibility(View.GONE);
                rootView.setBackground(ContextCompat.getDrawable(LoginActivity.super.getBaseContext(), R.drawable.background_color));
                startAnimation();
            }
        }.start();
    }

    private void loginUser() {
        emailEditText = findViewById(R.id.emailEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        String email = emailEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();

        loginPresenter.checkCredentials(email, password);
    }

    private void initViews(){
        loadingProgressBar = findViewById(R.id.loadingProgressBar);
        rootView = findViewById(R.id.rootView);
        zabavaNetLogo = findViewById(R.id.zabavaNetLogo);
        afterAnimationView = findViewById(R.id.afterAnimationView);
        register = findViewById(R.id.goToRegister);
        forgotPassword = findViewById(R.id.forgotPassword);
        loginButton = findViewById(R.id.loginButton);
    }

    private void startAnimation() {
        ViewPropertyAnimator viewPropertyAnimator = zabavaNetLogo.animate();
        viewPropertyAnimator.y(50f);
        viewPropertyAnimator.setDuration(1000);
        viewPropertyAnimator.setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                loginPresenter.onAnimationEnd();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                loginPresenter.onAnimationEnd();
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public void loginSuccess(String userType) {
        Toast.makeText(getApplicationContext(), "Login successful.", Toast.LENGTH_SHORT).show();
        passwordEditText.setText("");
        emailEditText.setText("");
        Intent intent = new Intent(getApplicationContext(),
                userType.equals("visitor") ? EventFeedVisitorActivity.class
                        : EventFeedOrganiserActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void loginFail() {
        Toast.makeText(getApplicationContext(), "Login failed, please try again.", Toast.LENGTH_SHORT).show();
        passwordEditText.setText("");
        emailEditText.setText("");
    }

    public void emailErrorEmptyO() {
        emailEditText.setError("Email is required");
        emailEditText.requestFocus();
    }

    public void emailErrorPatternO() {
        emailEditText.setError("Please enter a valid email");
        emailEditText.requestFocus();
    }

    public void passwordErrorEmptyO() {
        passwordEditText.setError("Password is required");
        passwordEditText.requestFocus();
    }

    public void passwordErrorLengthO() {
        passwordEditText.setError("Minimum length of password is 6");
        passwordEditText.requestFocus();
    }

    public void redirectToHomeScreen(Map<String, Object> userType) {

        if(Objects.equals(userType.get("user-type"), "visitor") &&
        userType instanceof HashMap) {
            startActivity(new Intent(this, EventFeedVisitorActivity.class)
                    .putExtra("user", (HashMap<String, Object>) userType));
        } else if(Objects.equals(userType.get("user-type"), "organiser") &&
        userType instanceof HashMap) {
            startActivity(new Intent(this, EventFeedOrganiserActivity.class)
                    .putExtra("user", (HashMap<String, Object>) userType));
        } else {
            showLoginForm();
        }
    }

    public void showLoginForm() {
        afterAnimationView.setVisibility(View.VISIBLE);
    }
}
