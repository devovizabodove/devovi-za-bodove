package com.devovizabodove.zabavanet.models;
import com.devovizabodove.zabavanet.presenters.LoginPresenter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import androidx.annotation.NonNull;

public class LoginModel {
    private String email, password;

    private LoginPresenter loginPresenter;

    private FirebaseAuth mAuth;

    public LoginModel(LoginPresenter loginPresenter) {
        this.loginPresenter = loginPresenter;
        mAuth = FirebaseAuth.getInstance();

        // TODO: Remove before release
        mAuth.signOut();
    }

    public void login() {
        mAuth.signInWithEmailAndPassword(email, password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                if(authResult.getUser() != null)
                    db.collection("users").document(authResult.getUser().getUid()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            loginPresenter.loginOutcome(documentSnapshot.get("user-type"));
                        }
                    });
                else
                    loginPresenter.loginOutcome(null);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                loginPresenter.loginOutcome(null);
            }
        });
    }


    public String getEmail() {
        return email;
    }

    public String getPassword(){
        return password;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public void isUserLoggedIn() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        if(mAuth.getCurrentUser() != null) {
            DocumentReference userRef = db.collection("users")
                    .document(mAuth.getCurrentUser().getUid());

            userRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    loginPresenter.onUserDataComplete(documentSnapshot);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    loginPresenter.onUserDataComplete(null);
                }
            });
        } else {
            loginPresenter.onUserDataComplete(null);
        }
    }
}
