package com.devovizabodove.zabavanet;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseUser;

import java.util.Random;

public class OrganiserPaymentActivity extends AppCompatActivity {

    Button paypalButton;
    Button creditCardButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organiser_payment);
        Bundle b = getIntent().getExtras();

        paypalButton = findViewById(R.id.paypalButton);
        creditCardButton = findViewById(R.id.creditCardButton);

        paypalButton.setOnClickListener(getPaymentListener((FirebaseUser) b.get("user")));
        creditCardButton.setOnClickListener(getPaymentListener((FirebaseUser) b.get("user")));
    }


    private View.OnClickListener getPaymentListener(final FirebaseUser user){
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Random rand = new Random();
                final int rand_int = rand.nextInt(100);
                final ProgressDialog dialog = ProgressDialog.show(OrganiserPaymentActivity.this, "",
                        "Please wait, your transaction is being processed.", true);
                new CountDownTimer(5000, 1000){

                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        dialog.dismiss();
                        String msg;
                        View view = findViewById(R.id.paymentView);
                        if (rand_int > 10){
                            msg = "Transaction successful!";

                            new CountDownTimer(1000, 1000){

                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    OrganiserPaymentActivity.this.finish();
                                    startActivity(new Intent(OrganiserPaymentActivity.this, EventFeedOrganiserActivity.class));
                                }
                            }.start();
                        }
                        else {
                            msg = "Transaction failed. Please, try again.";
                        }
                        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT)
                                .show();
                    }
                }.start();
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, null);
    }
}
