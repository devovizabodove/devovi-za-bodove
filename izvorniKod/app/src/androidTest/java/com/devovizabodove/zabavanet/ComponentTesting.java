package com.devovizabodove.zabavanet;

import android.util.Log;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import androidx.test.rule.ActivityTestRule;
import androidx.viewpager.widget.ViewPager;

import com.devovizabodove.zabavanet.presenters.OrganiserRegisterPresenter;
import com.devovizabodove.zabavanet.presenters.VisitorRegisterPresenter;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class ComponentTesting {

    @Rule
    public ActivityTestRule<RegisterActivity> activityTestRule = new ActivityTestRule<>(RegisterActivity.class);

    private RegisterActivity registerActivity;
    private OrganiserRegisterFragment organiserRegisterFragment;
    private VisitorRegisterFragment visitorRegisterFragment;
    private ViewPager viewPager;

    @Before
    public void setUp() throws Exception{
        registerActivity = activityTestRule.getActivity();

        viewPager = registerActivity.findViewById(R.id.pager);

        visitorRegisterFragment = (VisitorRegisterFragment) registerActivity.getSupportFragmentManager().getFragments().get(viewPager.getCurrentItem());
        organiserRegisterFragment = (OrganiserRegisterFragment) registerActivity.getSupportFragmentManager().findFragmentById(R.id.pager);
    }

    @Test
    public void TC1_checkCredentials_ValidCredentials_Visitor() {
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        Date date = new Date();
        String username = "TC1_username";
        String email = "TC1_email" + formatter.format(date) + "@email.com";
        Log.d("TC1", "TC1_email" + formatter.format(date) + "@email.com");
        String password = "TC1_password";

        VisitorRegisterPresenter presenter = new VisitorRegisterPresenter(visitorRegisterFragment);

        String result = presenter.checkCredentials(username, email, password);

        assertEquals("Please wait, you are being registered.", result);
    }

    @Test
    public void TC2_checkCredentials_BlankEmail_Visitor() {
        String username = "TC2_username";
        String email = "";
        String password = "TC2_password";

        VisitorRegisterPresenter presenter = new VisitorRegisterPresenter(visitorRegisterFragment);

        String result = presenter.checkCredentials(username, email, password);

        assertEquals("Please enter a valid email", result);
    }

    @Test
    public void TC3_checkCredentials_ShortPassword_Visitor() {
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        Date date = new Date();
        String username = "TC3_username";
        String email = "TC3_email" + formatter.format(date) + "@email.com";
        Log.d("TC3", "TC3_email" + formatter.format(date) + "@email.com");
        String password = "pass";

        VisitorRegisterPresenter presenter = new VisitorRegisterPresenter(visitorRegisterFragment);

        String result = presenter.checkCredentials(username, email, password);

        assertEquals("Minimum length of password is 6", result);
    }
    @Test
    public void TC4_checkCredentials_InvalidMail_Visitor() {
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        Date date = new Date();
        String username = "TC4_username";
        String email = "TC4_email" + formatter.format(date) + "@invalidMail";
        Log.d("TC4", "TC4_email" + formatter.format(date) + "@invalidMail");
        String password = "TC4_password";

        VisitorRegisterPresenter presenter = new VisitorRegisterPresenter(visitorRegisterFragment);

        String result = presenter.checkCredentials(username, email, password);

        assertEquals("Please enter a valid email", result);
    }
    @Test
    public void TC5_checkCredentials_BlankName_Visitor() {
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        Date date = new Date();
        String username = "";
        String email = "TC5_email" + formatter.format(date) + "@email.com";
        Log.d("TC5", "TC5_email" + formatter.format(date) + "@email.com");
        String password = "TC5_password";

        VisitorRegisterPresenter presenter = new VisitorRegisterPresenter(visitorRegisterFragment);

        String result = presenter.checkCredentials(username, email, password);

        assertEquals("Name is required", result);
    }
    @Test
    public void TC6_checkCredentials_ValidCredentials_Organiser() {
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        Date date = new Date();
        String username = "TC6_username";
        String address = "TC6_address";
        String email = "TC6_email" + formatter.format(date) + "@email.com";
        Log.d("TC6", "TC6_email" + formatter.format(date) + "@email.com");
        String password = "TC6_password";

        OrganiserRegisterPresenter presenter = new OrganiserRegisterPresenter(organiserRegisterFragment);

        String result = presenter.checkCredentials(username, address, email, password);

        assertEquals("Registration successful, proceed to payment.", result);
    }
    @Test
    public void TC7_checkCredentials_BlankName_Organiser() {
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        Date date = new Date();
        String username = "";
        String address = "TC7_address";
        String email = "TC7_email" + formatter.format(date) + "@email.com";
        Log.d("TC7", "TC7_email" + formatter.format(date) + "@email.com");
        String password = "TC7_password";

        OrganiserRegisterPresenter presenter = new OrganiserRegisterPresenter(organiserRegisterFragment);

        String result = presenter.checkCredentials(username, address, email, password);

        assertEquals("Name is required", result);
    }
    @Test
    public void TC8_checkCredentials_BlankAddress_Organiser() {
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        Date date = new Date();
        String username = "TC8_username";
        String address = "";
        String email = "TC8_email" + formatter.format(date) + "@email.com";
        Log.d("TC8", "TC8_email" + formatter.format(date) + "@email.com");
        String password = "TC8_password";

        OrganiserRegisterPresenter presenter = new OrganiserRegisterPresenter(organiserRegisterFragment);

        String result = presenter.checkCredentials(username, address, email, password);

        assertEquals("Address is required", result);
    }

    @Test
    public void TC9_checkCredentials_BlankEmail_Organiser() {
        String username = "TC9_username";
        String address = "TC9_address";
        String email = "";
        String password = "TC9_password";

        OrganiserRegisterPresenter presenter = new OrganiserRegisterPresenter(organiserRegisterFragment);

        String result = presenter.checkCredentials(username, address, email, password);

        assertEquals("Please enter a valid email", result);
    }
    @Test
    public void TC10_checkCredentials_BlankPassword_Organiser() {
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
        Date date = new Date();
        String username = "TC10_username";
        String address = "TC10_address";
        String email = "TC10_email" + formatter.format(date) + "@email.com";
        Log.d("TC10", "TC10_email" + formatter.format(date) + "@email.com");
        String password = "";

        OrganiserRegisterPresenter presenter = new OrganiserRegisterPresenter(organiserRegisterFragment);

        String result = presenter.checkCredentials(username, address, email, password);

        assertEquals("Minimum length of password is 6", result);
    }

}